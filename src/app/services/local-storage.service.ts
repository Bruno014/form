import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  token;

  constructor(
    private storage: Storage
  ) {
    this.initDB();
   }

  private _storage: Storage | null = null;

  async initDB(){
    // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    const storage = await this.storage.create();
    this._storage = storage;
  }

  async saveToken(token){
    await this.storage.set('token', token);

    return "OK";
  }

  async getToken() {
    const token = await this.storage.get("token");

    return token;
  }
}
