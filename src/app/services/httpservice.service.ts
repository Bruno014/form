import { LocalStorageService } from './local-storage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {

  url1 = "http://localhost:3000/test/admin";
  url2 = "http://localhost:8080/auth/realms/Demo-Realm/protocol/openid-connect/token";
  cat;
  name;

  constructor(private http: HttpClient, private localStorage: LocalStorageService
              ) { }

  httprequest(producto, precio, token){

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      "Authorization": 'Bearer ' + `${token}`
    });

    const options = {
      headers,
    };

    this.url1 += "/?producto=" + producto + "&precio=" + precio;

    return this.http.get("http://localhost:3000/test/admin?producto=hola&precio=123", options);
  }

  httpRequestLogin(user, pass){
    const headers = new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded",
    });
    const options = {
      headers,
    };

    const request = new HttpParams()
      .set('username', `${user}`)
      .set('password', `${pass}`)
      .set('grant_type', 'password')
      .set('client_id', 'nodejs-microservice')
      .set('client_secret', '7e30afed-aaaf-40a7-a85a-97b8412eb9b9');
    return this.http.post(this.url2, request.toString(), options);
  }
}
