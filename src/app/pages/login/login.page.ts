import { LocalStorageService } from "./../../services/local-storage.service";
import { HttpserviceService } from "./../../services/httpservice.service";
import { Component, OnInit } from "@angular/core";
import jwt_decode from "jwt-decode";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  user = {
    usuario: null,
    contrasenia: null,
  };

  constructor(private http: HttpserviceService, private route: Router, private localStorageCTRL: LocalStorageService) {}

  ngOnInit() {}

  save() {
    this.http.httpRequestLogin(this.user.usuario, this.user.contrasenia).subscribe((data) => {
      var token = data["access_token"];
      var decoded = jwt_decode(token);

      let roles = decoded["resource_access"]["nodejs-microservice"]["roles"];

      if (roles.includes("admin")) {
        this.localStorageCTRL.saveToken(token).then((data) => {
          this.route.navigate(["/form1"]);
        });
      }
    });
  }
}
